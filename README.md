Sequence Pattern Analyzer
===============================

Sequence Pattern Analyzer takes in a single FASTA file of patterns and a directory of FASTA files to identify these patterns. When the program is run, it will take each line in every sample FASTA file and identify which sections of that line come from which sections of the pattern file. 

To use Sequence Pattern Analyzer at the command line: 

python seq_identifier.py -p <path to patterns.fa> -s <path to sample.fa> -d

####Donor Alleles File
Select a FASTA file that has the alleles that you would like to compare against. 

####Input Alleles Files
Select a directory that has the alleles that you would like to compare. This directory should contain only the FASTA files you wish to use for analysis. Other items in the folder may cause errors at runtime.

####Output Directory 
Select a directory for the program's output. This could be your desktop or documents folder.


####Minimum Pattern Length
Specifies the minimum length for a pattern to be considered.

####Minimum Gap Length
Specifies the minimum length for a gap before detecting another pattern.

####Maximum Gap Length
Specifies the maximum length for a gap before detecting another pattern.

####Run
Runs the program given the settings.

Written in Python 2.7.  Requires BioPython and PyQt4.